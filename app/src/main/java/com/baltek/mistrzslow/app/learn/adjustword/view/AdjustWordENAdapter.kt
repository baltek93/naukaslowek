package com.baltek.mistrzslow.app.learn.adjustword.view

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.learn.adjustword.view.AdjustWordENAdapter.Holder
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL

import java.util.ArrayList

class AdjustWordENAdapter(internal var wordENList: List<WordEN>, internal var mContext: Context, wordPLList: List<WordPL>) : RecyclerView.Adapter<Holder>() {
    internal var wordPLList: List<WordPL>? = null
    private var focusedItem = -1
    var isVisible = ArrayList<Boolean>()

    init {
        for (i in wordENList.indices) {
            isVisible.add(true)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_adjust, parent, false)

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.textWordEN!!.setText(wordENList[position].name)
        holder.backgroud!!.setTag(R.string.id_visible_recyclerview, isVisible[position])
        if (!isVisible[position]) {
            holder.backgroud!!.setVisibility(View.INVISIBLE)
        }
        holder.backgroud!!.setOnClickListener({ view ->
            if (focusedItem != -1)
                if (isVisible[focusedItem]) notifyItemChanged(focusedItem)
            focusedItem = holder.layoutPosition
            notifyItemChanged(focusedItem)
        })


        if (holder.backgroud!!.getTag(R.string.id_visible_recyclerview).equals(true)) {
            if (focusedItem == position) {
                holder.backgroud!!.setCardBackgroundColor(
                        mContext.resources.getColor(R.color.primary900))
                holder.backgroud!!.setTag(R.string.id_selected_recyclerview, "selected")
            } else {
                holder.backgroud!!.setCardBackgroundColor(
                        mContext.resources.getColor(R.color.primary300))
                holder.backgroud!!.setTag(R.string.id_selected_recyclerview, "noselected")
            }
        }
    }

    override fun getItemCount(): Int {
        return 8
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var textWordEN: TextView = view.findViewById(R.id.wordTV)
        internal var backgroud: CardView = view.findViewById(R.id.categories_background)

    }
}

