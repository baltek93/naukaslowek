package com.baltek.mistrzslow.app.search.view

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.annotation.Nullable
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.base.Navigator
import com.baltek.mistrzslow.app.base.Util
import com.baltek.mistrzslow.app.search.presenter.SearchDetailsPresenter
import com.baltek.mistrzslow.app.search.presenter.SearchPresenter
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL
import kotlinx.android.synthetic.main.fragment_search_details.*
import org.kodein.di.erased.instance

class SearchDetailsFragment : BaseFragment<SearchDetailsPresenter, SearchView>(), SearchView {

    override val presenter: SearchDetailsPresenter?
            by instance()
    private var wordAdapterPL: WordAdapterPL? = null
    private var wordAdapterEN: WordAdapterEN? = null
    private var wordPLs: List<WordPL>? = null
    private var isFirstRunning = true
    private var position: Int = 0
    private lateinit var searchEditText: EditText
    private lateinit var searchButton: ImageButton
    override val layoutId: Int
        get() = R.layout.fragment_search_details

    override val menuSettings: SetMenu
        get() = SetMenu.LEFT

    override fun initInjection() {
    }
    override fun getAllWords(words: List<WordPL>) {
        if (isFirstRunning) {
            wordPLs = words
            val mLayoutManager = LinearLayoutManager(context)
            wordsList!!.layoutManager = mLayoutManager
            wordsList!!.itemAnimator = DefaultItemAnimator()
            if (position == 0) {
                wordAdapterPL = WordAdapterPL(context as Context, wordPLs!!)
                wordsList!!.adapter = wordAdapterPL

            } else {
                presenter!!.getWordsEN("")

            }
        }


    }

    override fun getWordsPL(wordPLs: List<WordPL>) {
        wordAdapterPL!!.words = wordPLs
        wordAdapterPL!!.notifyDataSetChanged()
    }

    override fun getWordsEN(wordENs: List<WordEN>) {
        wordAdapterEN = WordAdapterEN(context, wordPLs!!, wordENs)
        wordsList!!.adapter = wordAdapterEN


    }

    override fun onCreate(savedInstanceState: Bundle?) {
//        kodein.instance<Navigator>(activity)
        super.onCreate(savedInstanceState)
    }
    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        position = arguments!!.getInt("POSITION")
        val searchFragment = fragmentManager!!.findFragmentByTag("AAAA") as SearchFragment
        val toolbar = searchFragment.view!!.findViewById<Toolbar>(R.id.toolbar)
        val searchEditText = toolbar.findViewById<EditText>(R.id.search_editText)
        val searchButton = toolbar.findViewById<ImageButton>(R.id.search_button)

        searchEditText.addTextChangedListener(SearchTextWatcher())


        if (!Util.isOnlineOrConnecting(context)) {
            searchButton.visibility = View.INVISIBLE
        }
        searchButton.setOnClickListener({
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, activity!!.resources.getString(R.string.please_speak))
            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
            if (position == 0) {
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "pl-PL")
            } else {
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-EN")
            }

            startActivityForResult(intent, 2)
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            2 -> if (resultCode == RESULT_OK && data != null) {
                val result = data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                searchEditText.setText(result[0])
                searchEditText.setSelection(result[0].length)
                isFirstRunning = false
                if (position == 0) {
                    presenter!!.getWordsPL(searchEditText.text.toString())
                } else {
                    presenter!!.getWordsEN(searchEditText.text.toString())
                }
            }
        }
    }

    inner class SearchTextWatcher : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun afterTextChanged(editable: Editable) {
            if (position == 0) {
                presenter!!.getWordsPL(editable.toString())
            } else {
                presenter!!.getWordsEN(editable.toString())
            }
        }
    }

    companion object {
        fun newInstance(position: Int): SearchDetailsFragment {
            val args = Bundle()
            val fragment = SearchDetailsFragment()
            args.putInt("POSITION", position)
            fragment.arguments = args
            return fragment
        }
    }
}
