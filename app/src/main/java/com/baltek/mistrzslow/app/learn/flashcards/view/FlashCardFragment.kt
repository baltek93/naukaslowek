package com.baltek.mistrzslow.app.learn.flashcards.view

import android.os.Bundle
import android.support.annotation.Nullable
import android.view.View
import android.widget.TextView
import com.baltek.mistrzslow.R

import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.base.MessageDialogFragment
import com.baltek.mistrzslow.app.base.Util
import com.baltek.mistrzslow.app.learn.flashcards.presenter.FlashCardPresenter
import com.baltek.mistrzslow.data.Model.WordPL

import java.util.Collections

import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskFragment.Companion.CATEGORIESID
import kotlinx.android.synthetic.main.fragmet_flash_cards.*
import org.kodein.di.erased.instance

class FlashCardFragment : BaseFragment<FlashCardPresenter, FlashcardView>(), FlashcardView {
    override val presenter: FlashCardPresenter? by instance()
    private var positiveAmount = 0
    private var negativeAmount = 0
    private var amountAllWords = 0
    private var wordPLRealmList: MutableList<WordPL>? = null
    private val mPositionQuestion = 0

    override val layoutId: Int
        get() = R.layout.fragmet_flash_cards

    internal fun onPositiveClick() {
        positiveAmount++
        setProgres()
        if (wordPLRealmList!!.size > 1) {
            wordPLRealmList!!.shuffle()
            words_text.setText(wordPLRealmList!![0].name)
        } else {

            Util.showDialog(fragmentManager!!, MessageDialogFragment.newInstance("Koniec", "Wiedziałeś $positiveAmount a nie wiedziales $negativeAmount"))
            showDialog("koniec")
            showEnglishWord.setText("Zakończ")
            //Todo created finish screen
        }
        wordPLRealmList!!.removeAt(0)
        check_anserw.visibility = View.GONE
        showEnglishWord.visibility = View.VISIBLE
    }

    internal fun onNegativeClick() {
        negativeAmount++
        setProgres()
        if (wordPLRealmList!!.size > 1) {
            wordPLRealmList!!.shuffle()
            words_text.setText(wordPLRealmList!![0].name)

        } else {
            Util.showDialog(fragmentManager!!, MessageDialogFragment.newInstance("Koniec", "Wiedziałeś $positiveAmount a nie wiedziales $negativeAmount"))
            showEnglishWord.setText("Zakończ")

            //Todo created finish screen
        }
        wordPLRealmList!!.removeAt(0)
        check_anserw.visibility = View.GONE
        showEnglishWord.visibility = View.VISIBLE
    }


    private fun onShowEnglish() {
        if (wordPLRealmList!!.size == 0) {
            activity!!.onBackPressed()
        } else {
            words_text.setText(wordPLRealmList!![0].wordEN!![0].name)
            showEnglishWord.visibility = View.GONE
            check_anserw.visibility = View.VISIBLE
        }

    }

    override fun initInjection() {
//        LearnInjection.getComponent(activity).inject(this)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_text_back, this.requireActivity())
//        (ButterKnife.findById(toolbar, R.id.toolbar_title) as TextView).setText("Sprawdź się")
        presenter!!.setCategoriesId(arguments!!.getLong(CATEGORIESID))
        positiveButton.setOnClickListener { onPositiveClick() }
        negativeButton.setOnClickListener { onNegativeClick() }
        showEnglishWord.setOnClickListener { onShowEnglish() }
    }

    override fun renderFlashCard(wordPLs: List<WordPL>) {
        wordPLRealmList = ArrayList()
        amountAllWords = wordPLs.size
        for (wPL in wordPLs)
            wordPLRealmList!!.add(wPL)
        Collections.shuffle(wordPLRealmList!!)
        wordPLRealmList!![mPositionQuestion]
        words_text.setText(wordPLRealmList!![mPositionQuestion].name)
    }

    private fun setProgres() {
        val f = (negativeAmount + positiveAmount).toDouble() / amountAllWords
        val progress = (f * 100).toInt()
        progressBar.progress = progress
        percentProgressTV.text = progress.toString() + "%"
    }

    companion object {

        fun newInstance(categoriesId: Long?): FlashCardFragment {
            val args = Bundle()
            val fragment = FlashCardFragment()
            args.putLong(CATEGORIESID, categoriesId!!)
            fragment.arguments = args
            return fragment
        }
    }
}
