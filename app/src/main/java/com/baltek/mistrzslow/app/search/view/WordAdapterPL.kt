package com.baltek.mistrzslow.app.search.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.baltek.mistrzslow.R

import com.baltek.mistrzslow.data.Model.WordPL


class WordAdapterPL(private val mContext: Context, var words: List<WordPL>) : RecyclerView.Adapter<WordAdapterPL.Holder>() {

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val textWordPL: TextView = view.findViewById(R.id.polish_word)
        val textWordEN: TextView = view.findViewById(R.id.english_word)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_search_words, parent, false)

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val word = words[position]
        holder.textWordPL!!.text = word.name
        val stringBuilder = StringBuilder()
        var amountWordEN = 0
        for (i in 0 until word.wordEN!!.size) {
            if (amountWordEN == 0) {
                stringBuilder.append(word.wordEN!![i].name)
                amountWordEN++
            } else {
                stringBuilder.append("\n" + word.wordEN!![i].name!!)

            }
        }
        holder.textWordEN!!.text = stringBuilder.toString()

        //holder.overflow.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        showPopupMenu(holder.overflow);
        //    }
        //});
    }

    ///**
    // * Showing popup menu when tapping on 3 dots
    // */
    //private void showPopupMenu(View view) {
    //    // inflate menu
    //    PopupMenu popup = new PopupMenu(mContext, view);
    //    MenuInflater inflater = popup.getMenuInflater();
    //    inflater.inflate(R.menu.menu_album, popup.getMenu());
    //    popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
    //    popup.show();
    //}
    //
    ///**
    // * Click listener for popup menu items
    // */
    //class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
    //
    //    public MyMenuItemClickListener() {
    //    }
    //
    //    @Override
    //    public boolean onMenuItemClick(MenuItem menuItem) {
    //        switch (menuItem.getItemId()) {
    //            case R.id.action_add_favourite:
    //                Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
    //                return true;
    //            case R.id.action_play_next:
    //                Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
    //                return true;
    //            default:
    //        }
    //        return false;
    //    }
    //}

    override fun getItemCount(): Int {
        return words.size
    }
}