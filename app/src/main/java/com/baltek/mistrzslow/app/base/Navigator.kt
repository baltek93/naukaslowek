package com.baltek.mistrzslow.app.base

import android.app.Activity
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

import com.baltek.mistrzslow.MainActivity
import com.baltek.mistrzslow.app.exam.view.CheckExamFragment
import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskFragment
import com.baltek.mistrzslow.app.learn.learnList.view.LearnListFragment
import com.baltek.mistrzslow.app.learn.listenwrite.view.ListenWriteFragment

class Navigator(val activity: Activity) {

    fun changeFragment(fragment: Fragment) {
        val transaction = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()

        transaction.replace(android.R.id.content, fragment)
        transaction.addToBackStack(null)

        transaction.commit()
    }

    fun changeActivity(activity: Activity, newActivity: Class<out Activity>) {
        activity.startActivity(Intent(activity, newActivity))
    }

    //
    //  public void navigateToExam() {
    //
    //    changeActivity(activity,ExamActivity.class);
    //  }
    //
    //  public void navigateToLearn() {
    //
    //    changeActivity(activity,LearnActivity.class);
    //  }
    fun navigateToSearch() {
        changeActivity(activity, MainActivity::class.java)
    }

        fun navigateToLearn() {

            changeFragment(LearnListFragment.newInstance(true))
        }

    fun navigateToExam() {

        changeFragment(LearnListFragment.newInstance(false))
    }

    fun navigateToCheckExam(id:Int) {

        changeFragment(CheckExamFragment.newInstance(id))
    }
    fun navigateToListenWrite(id:Long){
        changeFragment(ListenWriteFragment.newInstance(id))
    }
    fun navigateToChooseTask(id:Long){
        changeFragment(ChooseTaskFragment.newInstance(id))
    }
}
