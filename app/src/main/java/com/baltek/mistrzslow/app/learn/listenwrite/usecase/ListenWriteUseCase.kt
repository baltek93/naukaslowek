package com.baltek.mistrzslow.app.learn.listenwrite.usecase

import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase
import com.baltek.mistrzslow.data.Model.Categories
import com.baltek.mistrzslow.data.Model.ModelUtils
import com.baltek.mistrzslow.data.Model.WordPL
import io.reactivex.Observable
import io.realm.Sort
import rx.functions.Func0


/**
 * Created by Bartosz Kołtuniak on 13.12.2016.
 */

class ListenWriteUseCase(repository: Repository) : UseCase(repository) {

    fun getWordsEN(id: Long): Observable<Categories> {
        return queryLocal(Func0
        { repository.getListObservable(Categories::class.java, { t -> t.equalTo("id",id).findAll() }).map { mutableList -> mutableList[0] } }
                as Func0<Observable<Categories>>)
    }

}
