package com.baltek.mistrzslow.app.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import timber.log.Timber

abstract class BaseActivity<out PRESENTER : Presenter<VIEW>, VIEW : BaseView> : AppCompatActivity(), BaseView {

    protected abstract val presenter: PRESENTER

    override fun getStringFromId(id: Int): String {
        return getString(id)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
    }

    override fun getMainActivity(): Activity {
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //        getApp
//        ButterKnife.bind(this)
        presenter.onViewCreated(this as VIEW)
    }

    override fun showDialog(message: String) {
        Util.showDialog(supportFragmentManager, MessageDialogFragment.newInstance(message))
    }

    //@Override public void logoutUser(String errorMessage) {
    //  Intent intent = new Intent(this, AuthorizationActivity.class);
    //  intent.putExtra(ERROR_CODE_MESSAGE, errorMessage);
    //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    //  startActivity(intent);
    //}

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun log(message: String) {
        //        Log.d(TAG, message);
        Timber.d(message)
    }
}