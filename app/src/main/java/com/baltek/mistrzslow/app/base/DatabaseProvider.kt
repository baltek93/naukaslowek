package com.baltek.mistrzslow.app.base

import io.realm.Realm
import java.io.File

interface DatabaseProvider {
    fun getFile(id: String): File

    enum class Configuration {
        UI_THREAD, BACKGROUND_THREAD
    }

    fun getRealm(configuration: Configuration): Realm
}
