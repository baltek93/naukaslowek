package com.baltek.mistrzslow.app.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.baltek.mistrzslow.MainActivity
import com.baltek.mistrzslow.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
