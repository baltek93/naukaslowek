package com.baltek.mistrzslow.app.learn.learnList.view

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.learn.learnList.presenter.LearnListPresenter
import com.baltek.mistrzslow.app.learn.learnList.view.CategoriesAdapter.Holder
import com.baltek.mistrzslow.data.Model.Categories

import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class CategoriesAdapter
//private List<WordEN> wordsEN;

(private val mContext: Context, private val categories: List<Categories>, private val presenter: LearnListPresenter)//this.categories = categories;
    : RecyclerView.Adapter<Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_categories, parent, false)

        return Holder(itemView)
    }

    override fun onBindViewHolder(
            holder: Holder, position: Int) {
        val uri = categories[position].url


        // int imageResource = R.drawable.icon;
        val imageResource = mContext.resources.getIdentifier(uri, null, mContext.packageName)

        val image = mContext.resources.getDrawable(imageResource)
        holder.categoriesIV!!.setImageDrawable(image)
        holder.name!!.text = categories[position].name
        holder.categoriesBackground!!.setOnClickListener { view ->
            YoYo.with(Techniques.FlipInX)
                    .duration(700)
                    .playOn(view)
            presenter.showGames(position.toLong())
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var categoriesIV: ImageView = view.findViewById(R.id.categories_img)
        internal var categoriesBackground: CardView? = view.findViewById(R.id.categories_background)
        internal var name: TextView? = view.findViewById(R.id.name_categories)

        init {

        }
    }
}

