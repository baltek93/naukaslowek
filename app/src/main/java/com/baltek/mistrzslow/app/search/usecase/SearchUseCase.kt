package com.baltek.mistrzslow.app.search.usecase

import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL
import io.reactivex.Observable

import java.io.InputStream

import io.realm.Sort
import rx.functions.Func0
import rx.functions.Func1

class SearchUseCase(override val repository: Repository) : UseCase(repository) {
    private var myText: String? = null
    fun saveAll(stream: InputStream): Observable<Boolean> {
        return queryLocal(Func0 { repository.saveAll(stream) })
    }

    //  public Observable<Boolean> saveAll(InputStream stream) {
    //    return queryLocal(() -> getRepository().saveAll(stream));
    //  }

    fun getAllWords(): Observable<List<WordPL>> {
//        return queryLocal(Func0 { repository.getListObservable(WordPL::class.java) } as Func0<Observable<List<WordPL>>>)
        return queryLocal(Func0
        { repository.getListObservable(WordPL::class.java, { t -> t.findAll().sort("id", Sort.DESCENDING) }) }
                as Func0<Observable<List<WordPL>>>)
    }

    fun getWordPL(enterText: String): Observable<List<WordPL>> {
        myText = enterText.trim().replace("\\s{2,}", " ")
        myText = myText!!.toLowerCase()
        when {
            myText!!.endsWith(" ") -> {
                val firstText = myText!!.substring(0, myText!!.indexOf(" "))
                return queryLocal(Func0 { repository.getListObservable(WordPL::class.java, { wordPL -> wordPL.contains("name", firstText).findAll().sort("id", Sort.DESCENDING) }) } as Func0<Observable<List<WordPL>>>)
            }
            myText!!.contains(" ") -> {
                val firstText = myText!!.substring(0, myText!!.indexOf(" "))
                val twoText = myText!!.substring(myText!!.indexOf(" "), myText!!.lastIndex)
                return queryLocal(Func0 {
                    repository.getListObservable(WordPL::class.java,
                            { wordPL -> wordPL.contains("name", firstText).contains("name", twoText).findAll().sort("id", Sort.DESCENDING) })
                } as Func0<Observable<List<WordPL>>>)

            }
            else -> return queryLocal(Func0 { repository.getListObservable(WordPL::class.java, { wordPL -> wordPL.contains("name", myText).findAll().sort("id", Sort.DESCENDING) }) } as Func0<Observable<List<WordPL>>>)
        }
    }


    fun getWordEN(enterText: String): Observable<List<WordEN>> {
        myText = enterText.trim().replace("\\s{2,}", " ")
        myText = myText!!.toLowerCase()
        return queryLocal(Func0 { repository.getListObservable(WordEN::class.java, { wordPL -> wordPL.contains("name", myText).findAll().sort("id", Sort.DESCENDING) }) } as Func0<Observable<List<WordEN>>>)

    }
//  public Observable<List<WordEN>> getWordEN(String a) {
//    myText = a.trim().replaceAll("\\s{2,}", " ");
//    myText = myText.toLowerCase();
//    Observable<List<WordEN>> test= queryLocal(() -> getRepository().getListObservable(WordEN.class,
//        wordPLRealmQuery -> wordPLRealmQuery.contains("name", myText).findAll())
//        .map(o -> o));
//    return test;
//  }


}
