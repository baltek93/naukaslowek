package com.baltek.mistrzslow.app.exam.view

import com.baltek.mistrzslow.app.base.BaseView
import com.baltek.mistrzslow.data.Model.WordPL

interface CheckExamView : BaseView {
    fun renderQuestion(wordPLs: List<WordPL>)
}
