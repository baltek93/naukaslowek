package com.baltek.mistrzslow.app.learn.learnList.view

import com.baltek.mistrzslow.app.base.BaseView
import com.baltek.mistrzslow.data.Model.Categories


interface LearnListView : BaseView {
    fun renderCategories(categories: List<Categories>)

    fun renderGames(categorieId: Long)

    fun renderExam(categorieId: Long)
}
