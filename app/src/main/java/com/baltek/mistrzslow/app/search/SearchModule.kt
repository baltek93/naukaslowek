package com.baltek.mistrzslow.app.search

import com.baltek.mistrzslow.app.search.presenter.SearchDetailsPresenter
import com.baltek.mistrzslow.app.search.presenter.SearchPresenter
import com.baltek.mistrzslow.app.search.usecase.SearchUseCase
import com.baltek.mistrzslow.app.search.view.SearchFragment
import org.kodein.di.Kodein
import org.kodein.di.erased.bind
import org.kodein.di.erased.instance
import org.kodein.di.erased.provider
import org.kodein.di.erased.singleton

val searchKodein = Kodein.Module {
    bind<SearchUseCase>() with provider { SearchUseCase(instance()) }
    bind<SearchPresenter>() with provider { SearchPresenter(instance()) }
    bind<SearchDetailsPresenter>() with provider { SearchDetailsPresenter(instance())
    }
}