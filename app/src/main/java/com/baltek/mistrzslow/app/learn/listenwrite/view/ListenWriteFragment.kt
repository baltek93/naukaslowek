package com.baltek.mistrzslow.app.learn.listenwrite.view

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.v4.app.FragmentManager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import com.baltek.mistrzslow.R

import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.base.MessageDialogFragment
import com.baltek.mistrzslow.app.base.Util
import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskFragment.Companion.CATEGORIESID
import com.baltek.mistrzslow.app.learn.listenwrite.presenter.ListenWritePresenter
import com.baltek.mistrzslow.data.Model.Categories
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL
import java.util.Collections
import java.util.Locale

import io.realm.RealmList
import io.realm.WordENRealmProxy
import kotlinx.android.synthetic.main.listen_content.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.erased.instance


/**
 * Created by Bartosz Kołtuniak on 13.12.2016.
 */

class ListenWriteFragment : BaseFragment<ListenWritePresenter, ListenWriteView>(), ListenWriteView {


    override val presenter: ListenWritePresenter?
            by instance()
    private var mTextToSpeech: TextToSpeech? = null
    private var mWordEN: MutableList<WordEN> = mutableListOf()
    private lateinit var mWordPL: MutableList<WordPL>
    private var amountWordsAll: Int = 0
    private var amountWordsCorrect: Int = 0
    private var amountWordsInCorrect: Int = 0

    override val layoutId: Int
        get() = R.layout.fragment_listen_write

    private fun onSubmitClick() {
        if (mWordEN.size > 1) {
            presenter!!.renderNextQuestion(anserw.text.toString())
        } else if (mWordEN.size === 1) {
            if (mWordEN[0].name.equals(anserw!!.text.toString())) {
                amountWordsCorrect++
                mWordEN.removeAt(0)
            } else {
                mWordEN.add(mWordEN[0])
                amountWordsInCorrect++
                amountWordsAll++
            }
            setProgres()
            Util.showDialog(fragmentManager as FragmentManager, MessageDialogFragment.newInstance("Wynik",
                    " Twój wynik to " + amountWordsCorrect + " na " + (amountWordsCorrect + amountWordsInCorrect)))
            submit_btn!!.text = "Zakończ"
        } else {
            activity!!.onBackPressed()
        }
    }

    private fun onMicClick() {
        renderSpeechInput(mWordEN[0].name as String)
    }

    override fun initInjection() {
//        LearnInjection.getComponent(activity).inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_text_back, this.requireActivity())

//        (ButterKnife.findById(activity, R.id.toolbar_title) as TextView).text = "Słuchaj i pisz"
        presenter!!.setCateogoriesId(arguments!!.getLong(CATEGORIESID))
        submit_btn.setOnClickListener { onSubmitClick() }
        speech_text.setOnClickListener { onMicClick()  }
    }

    override fun renderSpeechInput(text: String) {
        mTextToSpeech = TextToSpeech(activity) { i ->
            if (i != TextToSpeech.ERROR) {
                mTextToSpeech!!.language = Locale.ENGLISH
                mTextToSpeech!!.speak(text, TextToSpeech.QUEUE_FLUSH, null)
            }
        }
    }

    override fun rendeNextQuestion(text: String) {
        if (   (mWordEN[0] as WordENRealmProxy).`realmGet$name`()== text.toLowerCase()) {
            amountWordsCorrect++
            mWordEN.removeAt(0)
            renderSpeechInput(mWordEN[0].name)
        } else {
            mWordEN.add(mWordEN[0])
            amountWordsInCorrect++
            amountWordsAll++
        }
        setProgres()
        mWordEN.shuffle()
        renderSpeechInput(mWordEN[0].name)
    }

    override fun saveWords(categories: Categories) {
        mWordPL = categories.wordPLs!!
//        mWordEN = RealmList<WordEN>()
        for (wordPL in mWordPL) {
            for (wordEN in wordPL.wordEN!!)
                mWordEN.add(wordEN)
        }
        for (i in 0 until mWordEN.size) {
            var j = i + 1
            while (j < mWordEN.size) {
                if (mWordEN[i] == mWordEN[j]) {
                    mWordEN.removeAt(j)
                    j--
                }
                j++
            }
        }
        Collections.shuffle(mWordEN)
        presenter!!.renderSpeechInput(mWordEN[0].name)
        amountWordsAll = mWordEN.size
        amountWordsCorrect = 0
        amountWordsInCorrect = 0
        setProgres()
    }

    override fun onPause() {
        if (mTextToSpeech != null) {
            mTextToSpeech!!.stop()
            mTextToSpeech!!.shutdown()
        }
        super.onPause()
    }

    private fun setProgres() {
        val f = amountWordsCorrect.toDouble() / amountWordsAll
        val progress = (f * 100).toInt()
        progressBar!!.progress = progress
        percentProgressTV!!.text = progress.toString() + "%"
    }

    companion object {
        fun newInstance(categoriesId: Long?): ListenWriteFragment {
            val args = Bundle()
            val fragment = ListenWriteFragment()
            args.putLong(CATEGORIESID, categoriesId!!)
            fragment.arguments = args
            return fragment
        }

    }
}
