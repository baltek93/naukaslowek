package com.baltek.mistrzslow.app.learn.learnList.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.learn.learnList.usecase.LearnListUseCase
import com.baltek.mistrzslow.app.learn.learnList.view.LearnListView


class LearnListPresenter(private var useCase: LearnListUseCase) : Presenter<LearnListView>(useCase) {

    var isLearn:Boolean = true
    override fun onViewVisible() {
        super.onViewVisible()
        query(useCase.getCategory().subscribe({ categories -> getView().renderCategories(categories) }))
    }

    fun showGames(position: Long) {
        if(isLearn)
        {
            getView().renderGames(position)
        }
        else
        {
            getView().renderExam(position)
        }
    }

}
