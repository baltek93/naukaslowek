package com.baltek.mistrzslow.app.learn.adjustword.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.learn.adjustword.usecase.AdjustWordUseCase
import com.baltek.mistrzslow.app.learn.adjustword.view.AdjustWordView

class AdjustWordPresenter(internal var usecase: AdjustWordUseCase) : Presenter<AdjustWordView>(usecase) {
    private var categoriesId: Long = 0

    override fun onViewVisible() {
        super.onViewVisible()
        query(usecase.getWordPL(categoriesId).subscribe { wordPLs -> getView().renderQuestion(wordPLs) })

    }

    fun setCategoriesId(categoriesId: Long) {
        this.categoriesId = categoriesId
    }
}
