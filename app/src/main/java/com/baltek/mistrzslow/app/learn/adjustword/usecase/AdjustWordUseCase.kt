package com.baltek.mistrzslow.app.learn.adjustword.usecase

import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase

class AdjustWordUseCase(repository: Repository) : UseCase(repository)
