package com.baltek.mistrzslow.app.learn.flashcards.view

import com.baltek.mistrzslow.app.base.BaseView
import com.baltek.mistrzslow.data.Model.WordPL

interface FlashcardView : BaseView {
    fun renderFlashCard(wordPLs: List<WordPL>)
}
