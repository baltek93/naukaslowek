package com.baltek.mistrzslow.app.base

import android.app.Activity
import android.content.Context
import android.support.design.widget.NavigationView
import android.support.v4.app.ActionBarDrawerToggle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewStub
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toolbar
import com.baltek.mistrzslow.R

class DrawerManager constructor(private val menuCustomClickListener: MenuCustomClickListener) {

    lateinit var drawerLayout: DrawerLayout
    lateinit var mMenuRecyclerViewLeft: RecyclerView

    fun inflateMenu(inflater: LayoutInflater, layoutId: Int): View {
        val view: View = inflater.inflate(R.layout.navigation_drawer, null)
        drawerLayout = view.findViewById(R.id.drawer_layout)
        mMenuRecyclerViewLeft = view.findViewById(R.id.nav_list_left)

        val nv = view.findViewById<View>(R.id.navigation_left) as NavigationView
        val fl = view.findViewById<View>(R.id.root_frame) as ViewStub
        fl.layoutResource = layoutId
        fl.inflate()
        attachAdapterMenu(view.context)
        drawerLayout!!.addDrawerListener(menuCustomClickListener)
        return view
    }

    fun inflateToolbar(view: View, resourceId: Int, activity: Activity): View {
        val stub = view.findViewById<View>(R.id.toolbar_stub) as ViewStub
        stub.layoutResource = resourceId
        val inflated = stub.inflate()
        val button = inflated.findViewById<View>(R.id.home_button)
        //View button = inflated.findViewById(homeId);
        button?.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }

        val backButton = view.findViewById<ImageButton>(R.id.back_button)
        if (backButton != null) backButton!!.setOnClickListener({ activity.onBackPressed() })

        return inflated
    }

    private fun attachAdapterMenu(context: Context) {
        val mAdapterLeft = NavigationAdapter(this, context, R.menu.menu_drawer_left, R.id.nav_camera,
                R.id.nav_camera, R.layout.nav_list_item, menuCustomClickListener)

        val mLayoutManagerLeft = LinearLayoutManager(context)

        mMenuRecyclerViewLeft!!.layoutManager = mLayoutManagerLeft

        mMenuRecyclerViewLeft!!.itemAnimator = DefaultItemAnimator()

        mMenuRecyclerViewLeft!!.adapter = mAdapterLeft
    }

    fun setToolbarText(toolbar: Toolbar, s: String) {
        (toolbar.findViewById<View>(R.id.toolbar_title) as TextView).text = s
    }
}
