package com.baltek.mistrzslow.app.base

import android.app.Activity
import android.support.v4.widget.DrawerLayout
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

import com.baltek.mistrzslow.R

class MenuCustomClickListener(private val navigator: Navigator) : View.OnClickListener, DrawerLayout.DrawerListener {

    override fun onClick(v: View) {

        when (v.id) {

            R.id.nav_search -> navigator.navigateToSearch()
            R.id.nav_learn -> navigator.navigateToLearn()
            R.id.nav_exam -> navigator.navigateToExam()
        }//      case R.id.nav_learn:
        //        navigator.navigateToLearn();
        //        break;
        //      case R.id.nav_exam:
        //        navigator.navigateToExam();
        //        break;
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}

    override fun onDrawerOpened(drawerView: View) {
//        val inputMethodManager = navigator.activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputMethodManager.hideSoftInputFromWindow(navigator.activity.currentFocus!!.windowToken, 0)
    }

    override fun onDrawerClosed(drawerView: View) {
//        val inputMethodManager = navigator.activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputMethodManager.hideSoftInputFromWindow(navigator.activity.currentFocus!!.windowToken, 0)
    }

    override fun onDrawerStateChanged(newState: Int) {}
}
