package com.baltek.mistrzslow.app.learn.listenwrite.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.learn.listenwrite.usecase.ListenWriteUseCase
import com.baltek.mistrzslow.app.learn.listenwrite.view.ListenWriteView
import io.reactivex.rxkotlin.subscribeBy

class ListenWritePresenter(private val usecase: ListenWriteUseCase) : Presenter<ListenWriteView>(usecase) {
    private var cateogoriesId: Long = 1

    fun setCateogoriesId(id: Long=1) {
        this.cateogoriesId = id
    }

    override fun onViewVisible() {
        query(usecase.getWordsEN(cateogoriesId).subscribeBy(
                onNext = {println(it.id.toString() + "AAAAA")
                    getView().saveWords(it) },
                onError =  { it.printStackTrace() },
                onComplete = { println("Done!") }))
    }

    fun renderSpeechInput(s: String?) {
        getView().renderSpeechInput(s as String)
    }

    fun renderNextQuestion(text: String) {
        getView().rendeNextQuestion(text)
    }
}
