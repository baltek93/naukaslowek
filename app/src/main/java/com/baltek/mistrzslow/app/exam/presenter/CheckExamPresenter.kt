package com.baltek.mistrzslow.app.exam.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.exam.usecase.CheckExamUseCase
import com.baltek.mistrzslow.app.exam.view.CheckExamView

class CheckExamPresenter(internal var useCase: CheckExamUseCase) : Presenter<CheckExamView>(useCase) {
    private var mCategories: Long = 0

    override fun onViewVisible() {
        super.onViewVisible()
        query(useCase.getWordPL(mCategories).subscribe { wordPLs -> getView().renderQuestion(wordPLs) })
    }

    fun setmCategories(mCategories: Long) {
        this.mCategories = mCategories
    }
}