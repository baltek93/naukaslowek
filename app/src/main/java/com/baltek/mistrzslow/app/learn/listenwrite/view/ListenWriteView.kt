package com.baltek.mistrzslow.app.learn.listenwrite.view

import com.baltek.mistrzslow.app.base.BaseView
import com.baltek.mistrzslow.data.Model.Categories

/**
 * Created by Bartosz Kołtuniak on 13.12.2016.
 */

interface ListenWriteView : BaseView {
    fun renderSpeechInput(Text: String)
    fun rendeNextQuestion(Text: String)

    fun saveWords(categories: Categories)
}
