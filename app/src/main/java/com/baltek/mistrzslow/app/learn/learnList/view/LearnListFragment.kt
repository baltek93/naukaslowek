package com.baltek.mistrzslow.app.learn.learnList.view

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.TextView
import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskFragment
import com.baltek.mistrzslow.app.learn.learnList.presenter.LearnListPresenter
import com.baltek.mistrzslow.data.Model.Categories
import kotlinx.android.synthetic.main.fragment_learn.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.erased.instance

class LearnListFragment : BaseFragment<LearnListPresenter, LearnListView>(), LearnListView {
    override fun renderExam(categorieId: Long) {
        navigator.navigateToCheckExam(categorieId.toInt())
    }

    override val presenter: LearnListPresenter?   by instance()
    override val layoutId: Int
        get() = R.layout.fragment_learn

    override val menuSettings: SetMenu
        get() = SetMenu.LEFT

    override fun initInjection() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_standard_text, this.requireActivity())
        toolbar.findViewById<TextView>(R.id.toolbar_title).text = "Wybierz Kategorie"
        presenter!!.isLearn = arguments!!.getBoolean("ISLEARN")

    }

    override fun renderCategories(categories: List<Categories>) {
        val mLayoutManager = GridLayoutManager(getContext(), 3)
        list_categorie!!.layoutManager = mLayoutManager
        list_categorie!!.adapter = CategoriesAdapter(context as Context, categories, presenter as LearnListPresenter)
    }

    override fun renderGames(categorieId: Long) {
        navigator!!.changeFragment(ChooseTaskFragment.newInstance(categorieId))
    }

    companion object {
        fun newInstance(isLearn: Boolean): LearnListFragment {
            val args = Bundle()
            val fragment = LearnListFragment()
            args.putBoolean("ISLEARN", isLearn)
            fragment.arguments = args
            return fragment
        }
    }
}
