package com.baltek.mistrzslow.app.base

import android.content.Context
import android.content.res.XmlResourceParser
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.baltek.mistrzslow.R
import java.util.ArrayList
import org.xmlpull.v1.XmlPullParser

class NavigationAdapter(private val drawerManager: DrawerManager, private val mContext: Context,
                        menuRes: Int, private val contactId: Int, private val profileId: Int, private val layoutId: Int,
                        private val menuCustomClickListener: MenuCustomClickListener) : RecyclerView.Adapter<NavigationAdapter.ViewHolder>() {
    private var mList: MutableList<MenuDrawerItem>? = null

    init {
        parseXml(menuRes)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = mList!![position]

        holder.itemView.id = item.id
        holder.itemView.setOnClickListener { v ->
            if (drawerManager.drawerLayout != null) drawerManager.drawerLayout.closeDrawers()
            menuCustomClickListener.onClick(v)
        }

        holder.titleText.setText(item.label)
        holder.imageView.setImageResource(item.iconRes)
        //if (item.id == R.id.nav_contact) {
        if (item.id === contactId) {
            holder.divider.visibility = View.VISIBLE
        } else {
            holder.divider.visibility = View.GONE
            //if (item.id == R.id.nav_profile) {
            if (item.id === profileId) {
                holder.divider.visibility = View.VISIBLE
            } else {
                holder.divider.visibility = View.GONE
            }
        }
        //if(item.id == R.id.nav_faq || item.id == R.id.nav_tac || item.id == R.id.nav_logout){
        //  holder.imageView.setMaxWidth(40);
        //  holder.imageView.setMaxHeight(40);
        //  holder.imageView.setPadding(25, holder.imageView.getPaddingTop(), holder.imageView.getPaddingRight(), holder.imageView.getPaddingBottom());
        //  holder.titleText.setPadding(25, holder.titleText.getPaddingTop(), holder.titleText.getPaddingRight(),
        //      holder.titleText.getPaddingBottom());
        //} else {
        //  holder.imageView.setMaxWidth(50);
        //  holder.imageView.setMaxHeight(50);
        //  holder.imageView.setPadding(15, holder.imageView.getPaddingTop(), holder.imageView.getPaddingRight(), holder.imageView.getPaddingBottom());
        //  holder.titleText.setPadding(25, holder.titleText.getPaddingTop(), holder.titleText.getPaddingRight(),
        //      holder.titleText.getPaddingBottom());
        //}
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    private fun parseXml(menu: Int) {
        // use 0 id to indicate no menu (as specified in JavaDoc)
        mList = ArrayList()
        if (menu == 0) return
        try {
            val xpp = mContext.resources.getXml(menu)
            xpp.next()
            var eventType = xpp.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    val elemName = xpp.name
                    if (elemName == "item") {
                        val textId = xpp.getAttributeValue("http://schemas.android.com/apk/res/android", "title")
                        val iconId = xpp.getAttributeValue("http://schemas.android.com/apk/res/android", "icon")
                        val resId = xpp.getAttributeValue("http://schemas.android.com/apk/res/android", "id")
                        val item = MenuDrawerItem()
                        item.id = Integer.valueOf(resId.replace("@", ""))
                        item.activityName = xpp.getAttributeValue("http://schemas.android.com/apk/res/android",
                                "titleCondensed")
                        //showMenuItem(item.id)) {
                        item.iconRes = Integer.valueOf(iconId.replace("@", ""))
                        item.label = resourceIdToString(textId)
                        mList!!.add(item)
                    }
                }
                eventType = xpp.next()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        notifyDataSetChanged()
    }

    private fun resourceIdToString(text: String): String {
        if (!text.contains("@")) {
            return text
        } else {
            val id = text.replace("@", "")
            return mContext.resources.getString(Integer.valueOf(id))
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var imageView: ImageView = itemView.findViewById<View>(R.id.row_icon) as ImageView
        var titleText: TextView = itemView.findViewById<View>(R.id.row_title) as TextView
        var messageNumberText: TextView = itemView.findViewById<View>(R.id.messagesNumber) as TextView
        var divider: View = itemView.findViewById(R.id.divider_menu_left)

        init {

            //item = (LinearLayout) itemView.findViewById(R.id.menu_list_item);
        }
    }


    private class MenuDrawerItem {
        var id: Int = 0
        var iconRes: Int = 0
        var label: String? = null
        var messageAmount: String? = null
        var activityName: String? = null
    }
}
