package com.baltek.mistrzslow.app.learn.choosetask.usecase

import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase

/**
 * Created by Bartosz Kołtuniak on 11.12.2016.
 */

class ChooseTaskUseCase(repository: Repository) : UseCase(repository)
