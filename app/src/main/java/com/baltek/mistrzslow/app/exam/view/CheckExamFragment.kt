package com.baltek.mistrzslow.app.exam.view

import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.FragmentManager
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.base.MessageDialogFragment
import com.baltek.mistrzslow.app.base.Util
import com.baltek.mistrzslow.app.exam.presenter.CheckExamPresenter
import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskFragment.Companion.CATEGORIESID
import com.baltek.mistrzslow.data.Model.WordPL
import kotlinx.android.synthetic.main.fragment_check_exam.*
import org.kodein.di.erased.instance
import java.util.*

class CheckExamFragment : BaseFragment<CheckExamPresenter, CheckExamView>(), CheckExamView {
    override fun initInjection() {

    }

    override val presenter: CheckExamPresenter? by instance()
    private var amountAllWords: Int = 0
    private var positiveAnserw = 0
    private lateinit var wordPLRealmList: ArrayList<WordPL>

    override val layoutId: Int
        get() = R.layout.fragment_check_exam

    private val result: Int
        get() {
            val f = positiveAnserw.toDouble() / amountAllWords
            return (f * 100).toInt()
        }

    private fun onNextQuestion() {
        when {
            wordPLRealmList.size == 0 -> activity!!.onBackPressed()
            wordPLRealmList.size == 1 -> {
                Util.showDialog(fragmentManager as FragmentManager, MessageDialogFragment.newInstance("Wynik", "Uzyskałeś $result%"))
                nextQuestionButton!!.text = "Zakończ"
                wordPLRealmList.removeAt(0)
                setProgres()

            }
            else -> {
                if (anserwET!!.text.toString().toLowerCase() == wordPLRealmList[0].wordEN!![0].name) {
                    positiveAnserw++
                }
                wordPLRealmList.removeAt(0)
                words_text!!.text = wordPLRealmList[0].name
                setProgres()


            }
        }
        anserwET!!.setText("")

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_text_back, this.requireActivity())
//        (ButterKnife.findById(toolbar, R.id.toolbar_title) as TextView).text = "Egzamin"
        presenter!!.setmCategories(arguments!!.getLong(CATEGORIESID))
        nextQuestionButton.setOnClickListener { onNextQuestion() }
    }

    override fun renderQuestion(wordPLs: List<WordPL>) {
        wordPLRealmList = ArrayList()
        amountAllWords = wordPLs.size
        for (wPL in wordPLs)
            wordPLRealmList.add(wPL)
        wordPLRealmList.shuffle()
        words_text!!.text = wordPLRealmList[0].name
    }

    private fun setProgres() {
        val f = (amountAllWords - wordPLRealmList.size).toDouble() / amountAllWords
        val progress = (f * 100).toInt()
        progressBar!!.progress = progress
        percentProgressTV!!.text = progress.toString() + "%"
    }

    companion object {
        fun newInstance(categorieId: Int): CheckExamFragment {
            val args = Bundle()
            val fragment = CheckExamFragment()
            args.putLong(CATEGORIESID, categorieId.toLong())
            fragment.arguments = args
            return fragment
        }
    }
}
