package com.baltek.mistrzslow.app.base

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.support.v4.app.DialogFragment
import com.baltek.mistrzslow.R

class MessageDialogFragment : DialogFragment() {

    lateinit var mTitleText: TextView
    lateinit var mMessageText: TextView
    lateinit var mButtonOk: Button
    private var onDismissListener: OnDismissListener? = null

    override fun onCreateDialog(args: Bundle?): Dialog {
        val title = arguments!!.getString(TITLE)
        val inflater = activity!!.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_message, null)
        mTitleText = dialogView.findViewById(R.id.dialog_title)
        mTitleText.text = title ?: "Message"
        mMessageText = dialogView.findViewById(R.id.dialog_message)
        mMessageText.text = arguments!!.getString(MESSAGE)
        mButtonOk = dialogView.findViewById(R.id.dialog_btn_ok)

        val dialog = Dialog(activity!!, R.style.MyDialogTheme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(dialogView)
        mButtonOk.setOnClickListener {
            dialog?.dismiss()
        }
        return dialog
    }

    fun setOnDismissListener(l: OnDismissListener) {
        onDismissListener = l
    }

    override fun onDismiss(di: DialogInterface?) {
        super.onDismiss(di)

        if (onDismissListener != null) {
            onDismissListener!!.onDismiss()
        }
    }

    interface OnDismissListener {
        fun onDismiss()
    }

    companion object {
        fun newInstance(title: String, message: String): MessageDialogFragment {
            val f = MessageDialogFragment()
            val args = Bundle()
            args.putString(TITLE, title)
            args.putString(MESSAGE, message)
            f.arguments = args
            return f
        }
        val TAG = "MessageDialogFragment"

        val ONLY_MESSAGE = 1
        val YES_NO = 2

        internal val TITLE = "title"
        internal val MESSAGE = "message"
        internal val TYPE = "dialogType"

        fun newInstance(message: String): MessageDialogFragment {
            val f = MessageDialogFragment()
            val args = Bundle()
            args.putString(MESSAGE, message)
            f.arguments = args
            return f
        }



        fun newInstance(title: String, message: String, dialogType: Int): MessageDialogFragment {
            val f = MessageDialogFragment()
            val args = Bundle()
            args.putString(TITLE, title)
            args.putString(MESSAGE, message)
            args.putInt(TYPE, dialogType)
            f.arguments = args
            return f
        }
    }
}

