package com.baltek.mistrzslow.app.search.presenter;

import com.baltek.mistrzslow.app.base.Presenter;
import com.baltek.mistrzslow.app.search.usecase.SearchUseCase;
import com.baltek.mistrzslow.app.search.view.SearchView;
import com.baltek.mistrzslow.data.Model.WordPL;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class SearchPresenter extends Presenter<SearchView> {
    SearchUseCase useCase;

    public SearchPresenter(SearchUseCase useCase) {
        super(useCase);
        this.useCase = useCase;
    }

    @Override
    public void onViewAttached() {
    if(getView()!=null)
    {

      try {
        query(useCase.saveAll(getView().getMainActivity().getAssets().open("words.json")).subscribe(aBoolean -> {
          if(aBoolean)
          {
            query(useCase.getAllWords().subscribe(categories -> {
              getAllWords(categories);
            }));
          }
        }));
      } catch (IOException e) {
      }
    }
    }

    public void getAllWords(List<WordPL> words) {
        if (getView() != null) {
            getView().getAllWords(words);
        }
    }


}
