package com.baltek.mistrzslow.app.base

import android.app.Activity
import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.baltek.mistrzslow.app.base.RealmDatabaseProvider.Companion.instance
import com.baltek.mistrzslow.app.exam.examModule
import com.baltek.mistrzslow.app.learn.learnKodein
import com.baltek.mistrzslow.app.search.searchKodein
import io.realm.Realm
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.androidModule
import org.kodein.di.android.androidScope
import org.kodein.di.direct
import org.kodein.di.erased.*


class MyApp : Application(), KodeinAware {
    override var kodein = Kodein.lazy {
        import(androidModule(this@MyApp))
//        bind<Application>() with singleton { MyApp() }
        bind<RealmDatabaseProvider>() with singleton {
            return@singleton RealmDatabaseProvider.init(instance())
        }

        bind<Realm>() with singleton {
            return@singleton Realm.getDefaultInstance()
        }
        bind<LocalRepository>() with provider { LocalRepository(instance()) }

        bind<Repository>() with provider { RepositoryService(instance()) }

        bind<MenuCustomClickListener>() with provider { MenuCustomClickListener(instance()) }
        bind<DrawerManager>() with provider { DrawerManager(instance()) }
        bind<Navigator>() with scoped(androidScope<Activity>()).singleton { Navigator(context) }

        import(searchKodein)
        import(learnKodein)
        import(examModule)
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

    }
}