package com.baltek.mistrzslow.app.base


import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.util.DisplayMetrics
import java.text.DateFormat
import java.util.ArrayList
import java.util.Date

object Util {

    private val replaceRegExp = "([\\\\\\[\\]\\^\\(\\)\\$\\.\\*\\+\\?\\{\\}\\|])"
    //private static String regExpSpecialChars = "\\[]^()$.*+?{}|";

    private val replacement = "\\\\$1"

    fun getStatusBarHeight(context: Context): Int {
        var result = 0
        val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun convertPixelsToDp(px: Float): Float {
        val metrics = Resources.getSystem().displayMetrics
        val dp = px / (metrics.densityDpi / 160f)
        return Math.round(dp).toFloat()
    }

    fun pixelsToSp(px: Float): Float {
        val scaledDensity = Resources.getSystem().displayMetrics.scaledDensity
        return px / scaledDensity
    }

    fun convertDpToPixel(dp: Float): Float {
        val metrics = Resources.getSystem().displayMetrics
        val px = dp * (metrics.densityDpi / 160f)
        return Math.round(px).toFloat()
    }


    /**
     * Checks whether the device is currently connected or is being connected to the Internet
     */
    fun isOnlineOrConnecting(context: Context?): Boolean {
        val cm = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }

    fun showDialog(fm: FragmentManager, df: DialogFragment) {
        val ft = fm.beginTransaction()
        val prev = fm.findFragmentByTag(MessageDialogFragment.TAG) as DialogFragment
        if (prev != null) {
            prev.dismiss()
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        df.show(ft, MessageDialogFragment.TAG)
    }

    fun normalizeAngle(angle: Float): Float {
        var newAngle = angle
        while (newAngle <= -180) newAngle += 360f
        while (newAngle > 180) newAngle -= 360f
        return newAngle
    }

    fun isEmpty(s: String?): Boolean {
        return s == null || s.length == 0
    }


    fun escapeAllSpecialRegRxpChars(rawValue: String): String {
        return rawValue.replace(replaceRegExp.toRegex(), replacement)//escaping special chars in delimiter such as '|', '.', '*' etc.
    }

    fun toString(date: Date?, format: DateFormat): String? {
        return if (date == null) null else format.format(date)
    }


}
