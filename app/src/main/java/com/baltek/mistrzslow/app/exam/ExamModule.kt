package com.baltek.mistrzslow.app.exam

import com.baltek.mistrzslow.app.exam.presenter.CheckExamPresenter
import com.baltek.mistrzslow.app.exam.usecase.CheckExamUseCase
import com.baltek.mistrzslow.app.learn.learnList.presenter.LearnListPresenter
import com.baltek.mistrzslow.app.learn.learnList.usecase.LearnListUseCase
import com.baltek.mistrzslow.app.learn.listenwrite.presenter.ListenWritePresenter
import com.baltek.mistrzslow.app.learn.listenwrite.usecase.ListenWriteUseCase
import org.kodein.di.Kodein
import org.kodein.di.erased.bind
import org.kodein.di.erased.instance
import org.kodein.di.erased.provider


val examModule = Kodein.Module {
    bind<CheckExamUseCase>() with provider { CheckExamUseCase(instance()) }
    bind<CheckExamPresenter>() with provider { CheckExamPresenter(instance()) }

}
