package com.baltek.mistrzslow.app.base

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import java.io.File

class RealmDatabaseProvider : DatabaseProvider {

    private var mRealm: Realm? = null
    private var context: Context? = null

    override fun getFile(id: String): File {
        return java.io.File(context!!.filesDir.path, id)
    }

    override fun getRealm(configuration: DatabaseProvider.Configuration): Realm {
        return if (configuration === DatabaseProvider.Configuration.UI_THREAD) mRealm as Realm else Realm.getDefaultInstance()
    }

    private object Holder {
        internal val INSTANCE = RealmDatabaseProvider()
    }

    companion object {

        fun init(context: Context): RealmDatabaseProvider {
            Realm.init(context)
            Realm.setDefaultConfiguration(
                    RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build())
            instance.mRealm = Realm.getDefaultInstance()
            instance.context = context
            return instance
        }

        val instance: RealmDatabaseProvider
            get() = RealmDatabaseProvider.Holder.INSTANCE
    }
}
