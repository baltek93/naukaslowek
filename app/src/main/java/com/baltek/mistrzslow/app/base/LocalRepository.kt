package com.baltek.mistrzslow.app.base

import android.os.Looper
import com.baltek.mistrzslow.data.Model.Categories
import com.baltek.mistrzslow.data.Model.IdModel
import com.baltek.mistrzslow.data.Model.ModelUtils
import io.reactivex.Observable
import io.reactivex.functions.Predicate
import io.realm.*
import rx.functions.Func0
import rx.functions.Func1
import java.io.IOException
import java.io.InputStream
import java.util.ArrayList


class LocalRepository(private val databaseProvider: DatabaseProvider) : LocalRepositoryInterface {
    private val mTimeout: Long = 500

    override fun <T : RealmModel?> getListObservable(t: Class<T>?, filter: Func1<RealmQuery<T>, RealmResults<T>>?): io.reactivex.Observable<MutableList<T>> {
        if (Thread.currentThread() == Looper.getMainLooper().thread) {
            val where = databaseProvider.getRealm(DatabaseProvider.Configuration.UI_THREAD).where(t)
            if (filter != null) {
                return Observable.just(filter.call(where)).filter({ t -> t.isLoaded }) as Observable<MutableList<T>>
            }
            return where.findAllAsync().asObservable().filter { it.isLoaded } as io.reactivex.Observable<MutableList<T>>
        } else {
            val realm = databaseProvider.getRealm(DatabaseProvider.Configuration.BACKGROUND_THREAD)
            val where = realm.where(t)
            if (filter != null) {
                val call = filter.call(where)
                val result = realm.copyFromRealm(call)
                realm.close()
                return Observable.just(result)
            }
            val all = where.findAll()
            val result = realm.copyFromRealm(all)
            realm.close()
            return Observable.just(result)
        }

    }

    override fun <T : RealmModel?> getListObservable(t: Class<T>?): io.reactivex.Observable<out MutableList<T>> {
        return getListObservable(t, null)

    }

    override fun <T : RealmModel?> getObservable(t: Class<T>?, filter: Func1<RealmQuery<T>, RealmQuery<T>>?): io.reactivex.Observable<T> {
        if (Thread.currentThread() === Looper.getMainLooper().thread) {
            val where = databaseProvider.getRealm(DatabaseProvider.Configuration.UI_THREAD).where(t)
            if (filter != null) run {
                return Observable.just(filter.call(where).findFirstAsync()).filter({ RealmObject.isLoaded(it) })
            }
            else run { return Observable.just(where.findFirstAsync()).filter { RealmObject.isLoaded(it) } }

        } else {
            val realm = databaseProvider.getRealm(DatabaseProvider.Configuration.BACKGROUND_THREAD)

            val where = realm.where(t)
            var item = if (filter != null) {
                filter.call(where).findFirst()
            } else {
                where.findFirst()
            }
            if (item != null && RealmObject.isValid(item)) item = realm.copyFromRealm(item)
            realm.close()
            return Observable.just(item)
        }
    }

    override fun doFileExists(code: String): Boolean {
        return databaseProvider.getFile(code).exists()
    }

    override fun <T : RealmModel?> saveData(list: MutableList<T>?): io.reactivex.Observable<Boolean> {
        if (list == null) return Observable.just(true)
        return getRealm(func = Func1 { t ->
            run {
                if (list.isNotEmpty()) {
                    val item = list[0]
                    t.executeTransaction({ realm1 -> realm1.copyToRealmOrUpdate(list) })
                }
            }
            Observable.just(true)

        })
    }

    override fun <T : RealmModel?> saveData(item: T): io.reactivex.Observable<Boolean> {
        return getRealm(Func1 { realm ->
            realm.executeTransaction({ realm1 -> realm1.copyToRealmOrUpdate(item) }/*, realm::close*/)
            Observable.just(true)
        })
    }

    override fun <E : RealmModel?> getObservable(t: Class<E>?): io.reactivex.Observable<E> {
        return getObservable(t, null)

    }
//
//    override fun executeTransaction(action: Func1<Realm, Void>?): Func0<Void> {
//        return getRealmForFunction(Func1 { realm-> {
//            realm.executeTransaction(action.call)
//        },
//        null})
////        return null as Func0<Void>
//    }


    private fun getRealmForFunction(func: Func1<Realm, Func0<Void>>): Func0<Void> {
        return if (Thread.currentThread() === Looper.getMainLooper().thread) {
            func.call(databaseProvider.getRealm(DatabaseProvider.Configuration.UI_THREAD))
        } else {
            val realm = databaseProvider.getRealm(DatabaseProvider.Configuration.BACKGROUND_THREAD)
            val result = func.call(realm)
            realm.close()
            result
        }
    }

    override fun <T : RealmModel?> deleteData(tClass: Class<T>?, capture: MutableList<IdModel>?): io.reactivex.Observable<Boolean> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveAll(stream: InputStream?): io.reactivex.Observable<Boolean> {
        return if (stream == null) Observable.just(true)
        else getRealm(Func1 { realm ->
            run {
                realm.executeTransaction({ realm1 ->
                    run {
                        realm1.createOrUpdateAllFromJson(Categories::class.java, stream)
                    }
                }
                )

            }
            Observable.just(true)

        })

//            getRealm({ realm ->
//
//                realm.executeTransaction({ realm1 ->
//                    try {
//                        realm1.createOrUpdateAllFromJson(Categories::class.java, stream)
//                    } catch (e: IOException) {
//                    }
//                })
//                Observable.just(true)
//            })
    }


    private fun <T> getRealm(func: Func1<Realm, Observable<T>>): Observable<T> {
        return if (Thread.currentThread() === Looper.getMainLooper().thread) {
            func.call(databaseProvider.getRealm(DatabaseProvider.Configuration.UI_THREAD))
        } else {
            val realm = databaseProvider.getRealm(DatabaseProvider.Configuration.BACKGROUND_THREAD)
            func.call(realm).map { t ->
                realm.close()
                t
            }
        }
    }
}
