package com.baltek.mistrzslow.app.learn.flashcards.usecase

import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase

class FlashCardUseCase(repository: Repository) : UseCase(repository)
