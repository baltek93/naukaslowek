package com.baltek.mistrzslow.app.exam.usecase

import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase

class CheckExamUseCase(repository: Repository) : UseCase(repository)
