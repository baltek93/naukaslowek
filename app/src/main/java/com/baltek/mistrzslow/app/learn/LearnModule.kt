package com.baltek.mistrzslow.app.learn

import com.baltek.mistrzslow.app.learn.adjustword.presenter.AdjustWordPresenter
import com.baltek.mistrzslow.app.learn.adjustword.usecase.AdjustWordUseCase
import com.baltek.mistrzslow.app.learn.choosetask.presenter.ChooseTaskPresenter
import com.baltek.mistrzslow.app.learn.choosetask.usecase.ChooseTaskUseCase
import com.baltek.mistrzslow.app.learn.flashcards.presenter.FlashCardPresenter
import com.baltek.mistrzslow.app.learn.flashcards.usecase.FlashCardUseCase
import com.baltek.mistrzslow.app.learn.learnList.presenter.LearnListPresenter
import com.baltek.mistrzslow.app.learn.learnList.usecase.LearnListUseCase
import com.baltek.mistrzslow.app.learn.listenwrite.presenter.ListenWritePresenter
import com.baltek.mistrzslow.app.learn.listenwrite.usecase.ListenWriteUseCase
import org.kodein.di.Kodein
import org.kodein.di.erased.bind
import org.kodein.di.erased.instance
import org.kodein.di.erased.provider

val learnKodein = Kodein.Module {
    bind<ListenWriteUseCase>() with provider { ListenWriteUseCase(instance()) }
    bind<ListenWritePresenter>() with provider { ListenWritePresenter(instance()) }
    bind<LearnListUseCase>() with provider { LearnListUseCase(instance()) }
    bind<LearnListPresenter>() with provider { LearnListPresenter(instance()) }
    bind<ChooseTaskUseCase>() with provider { ChooseTaskUseCase(instance()) }
    bind<ChooseTaskPresenter>() with provider { ChooseTaskPresenter(instance()) }
    bind<AdjustWordPresenter>() with provider { AdjustWordPresenter(instance()) }
    bind<AdjustWordUseCase>() with provider { AdjustWordUseCase(instance()) }
    bind<FlashCardPresenter>() with provider { FlashCardPresenter(instance()) }
    bind<FlashCardUseCase>() with provider { FlashCardUseCase(instance()) }

}
