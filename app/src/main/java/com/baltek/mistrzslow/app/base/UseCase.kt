package com.baltek.mistrzslow.app.base


import com.baltek.mistrzslow.data.Model.Categories
import com.baltek.mistrzslow.data.Model.ModelUtils
import com.baltek.mistrzslow.data.Model.WordPL
import com.fernandocejas.frodo.annotation.RxLogObservable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import rx.functions.Func0


//open class UseCase(private val repository: Repository) {
open class UseCase(protected open val repository: Repository){

    fun <T : Any> queryLocal(func: Func0<Observable<T>>): Observable<T> {
        return getObservable(func = func).observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
    }


    @RxLogObservable
    private fun <T:Any> getObservable(func: Func0<Observable<T>>): Observable<T> {
        return func.call()
    }


//    fun saveData(`object`: RealmObject): Observable<Boolean> {
//        return queryLocal({ repository.saveData(`object`).first() })
//    }
//
//    fun saveData(`object`: RealmModel): Observable<Boolean> {
//        return queryLocal({ repository.saveData(`object`).first() })
//    }
//
    fun getWordPL(id: Long): Observable<List<WordPL>> {


    return queryLocal(Func0
    { repository.getListObservable(Categories::class.java, { t -> t.equalTo("id",id).findAll() }).map{mutableList -> mutableList[0] } }
            as Func0<Observable<Categories>>).map { t ->t.wordPLs  }
//        return queryLocal({
//            repository.getObservable(Categories::class.java) { wordPLRealmQuery ->
//                wordPLRealmQuery.equalTo(
//                        ModelUtils.STANDARD_PRIMARY_KEY, id)
//            }.map(({ Categories.getWordPLs() }))

    }

}
