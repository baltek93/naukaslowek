package com.baltek.mistrzslow.app.learn.learnList.usecase


import com.baltek.mistrzslow.app.base.Repository
import com.baltek.mistrzslow.app.base.UseCase
import com.baltek.mistrzslow.data.Model.Categories
import com.baltek.mistrzslow.data.Model.IdModel
import com.baltek.mistrzslow.data.Model.WordPL
import io.reactivex.Observable
import io.realm.Sort
import rx.functions.Func0
import rx.functions.Func1

class LearnListUseCase(repository: Repository) : UseCase(repository) {

    fun getCategory(): Observable<List<Categories>> {
        return queryLocal(Func0
        { repository.getListObservable(Categories::class.java, { t -> t.findAll().sort("id", Sort.DESCENDING) }) }
                as Func0<Observable<List<Categories>>>)
    }

    fun saveIdModel(id: Int): Observable<Boolean> {
        return repository.saveData(IdModel(id.toLong()))
    }
}
