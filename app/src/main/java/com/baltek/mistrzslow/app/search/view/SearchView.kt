package com.baltek.mistrzslow.app.search.view

import com.baltek.mistrzslow.app.base.BaseView
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL

interface SearchView : BaseView {
    open fun getAllWords(categories: List<WordPL>)
    open fun getWordsPL(wordPLs: List<WordPL>)

    open fun getWordsEN(wordENs: List<WordEN>)
}
