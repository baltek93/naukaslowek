package com.baltek.mistrzslow.app.learn.flashcards.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.learn.flashcards.usecase.FlashCardUseCase
import com.baltek.mistrzslow.app.learn.flashcards.view.FlashcardView

class FlashCardPresenter(internal var usecase: FlashCardUseCase) : Presenter<FlashcardView>(usecase) {
    private var categoriesId: Long = 0

    fun setCategoriesId(categoriesId: Long) {
        this.categoriesId = categoriesId
    }

    override fun onViewVisible() {
        super.onViewVisible()
        query(usecase.getWordPL(categoriesId).subscribe { wordPLs -> getView().renderFlashCard(wordPLs) })
    }
}
