package com.baltek.mistrzslow.data.Model

object ModelUtils {
    val STANDARD_PRIMARY_KEY = "id"
    val STANDARD_SORT_KEY = "order"
    val DEBOUNCE_TIMEOUT = 100

}
