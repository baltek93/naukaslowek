package com.baltek.mistrzslow.data.Model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class WordPL(@PrimaryKey var id: Long = 1,
             var name: String? = null,
             var description: String? = null,
             var wordEN: RealmList<WordEN>? = null) : RealmObject()