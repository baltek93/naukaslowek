package com.baltek.mistrzslow.data.Model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Bartosz Kołtuniak on 06.11.2016.
 */
open class WordEN(@PrimaryKey
             var id: Long = 1,
             var name: String = "",
             var description: String = "") : RealmObject()