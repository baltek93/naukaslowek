package com.baltek.mistrzslow.data.Model

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class IdModel(@PrimaryKey
              var id: Long = 1) : RealmModel
