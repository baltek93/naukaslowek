package com.baltek.mistrzslow

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.baltek.mistrzslow.app.base.MyApp
import com.baltek.mistrzslow.app.base.Navigator
import com.baltek.mistrzslow.app.search.view.SearchFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.erased.instance

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val fragment = SearchFragment()
        fragmentTransaction.replace(android.R.id.content, fragment, "AAAA")
        fragmentTransaction.commit()
    }
}
